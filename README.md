## Install ##

- Clone or download repository
- Install Add-on via Firefox (currently not working!)
> NOTE: Instead type into Browser field "about:debugging" and choose "Add-on temporär laden". Choose File "manifest.json" from directory "webextension".
- Next run "install.bat" with administrator privileges 
- Example: To open folder "C:\test" from webpage create the following link on any webpage
```
<a href="file:///C:\test">Open Folder</a>
```
> NOTE: You'll receive an error on browser console since local files ("file:///") can not be opened via browser for security reasons. While the link will be blocked by firefox, the addon will pass it to a native app which calls the explorer.exe.


## Setup Devellopment Enviroment ##

### Install Python ###
- Download and install [Python 3.6](https://www.python.org/downloads/)
  Use "Add PATH-Variable to enviroment" from custom installation routine and reboot afer installation, test installtion via console:
```
python.exe -V
```
- Add [Python installer](http://www.pyinstaller.org/) witch enables to create windows-executable files from pyton scripts by taping into windows-console:
```
pip install pyinstaller
```
> NOTE: Currently only python 3.6 supported.

- Move into "pyinstaller_ceck"-directory and check pyinstaller funcionality by compiling an exe-file
```
cd pyinstaller_ceck
pyinstaller pyinstaller_check.py
```

### Install Native App in DevMode ###
- Edit absolute paths to devellopment directorys in following files:
*..\open-local-folder\0_install_dev.bat*
*..\open-local-folder\open_local_folder_dev.bat*
*..\open-local-folder\open_local_folder_dev.json*

- Run *install_dev.bat* to register native app in devellopment version


### Check for Errors ###
- The following Scrips checks, if anything is correct arranged  (only available in devellopment enviroment)
```
python check_config_win.py
```

## Further Links ##
- [Github Webextension examples](https://github.com/mdn/webextensions-examples)
- [Explanation for native messaging](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Native_messaging#App_manifest)



