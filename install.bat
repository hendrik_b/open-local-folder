@rem set current directory for abdministrator mode 
pushd "%~dp0"

@rem copy compiled python app to program folder path
rd /s/q "%ProgramFiles%\open_local_folder\"
xcopy "%cd%\native-app-win\dist\open_local_folder" "%ProgramFiles%\open_local_folder\" /s /e

@rem copy manifestfile to program folder path
copy "%cd%\native-app-win\open_local_folder.json" "%ProgramFiles%\open_local_folder\open_local_folder.json"

@rem REGKEY zur manifestdatei.json der app
@rem "...\NativeMessagingHosts\<name>" wobei der <name> unter dem app_maifest.json ganz oben steht-> "name" : "...."

REG ADD "HKEY_CURRENT_USER\SOFTWARE\Mozilla\NativeMessagingHosts\open_local_folder" /ve /t REG_SZ /d "%ProgramFiles%\open_local_folder\open_local_folder.json" /f

PAUSE