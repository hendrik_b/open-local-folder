
/* Pass received message on to the native app */
function notify(message) {

  // Messages passed only for local files 
  if (message.url.substring(0, 8) != 'file:///') return;

  // Remove obsolete charcters
  folder_path = message.url.replace('file:///','');
  // Replace %20 with space
  folder_path = folder_path.replace('%20',' ');
  // Send folder path to file to app
  console.log("background script message passed: " + folder_path);
  port.postMessage(folder_path);

}

/* On startup, connect to the "crp_folder_open" app. */
var port = browser.runtime.connectNative("open_local_folder");

/* Listen for messages from the app. */
port.onMessage.addListener((response) => {
  console.log("Received: " + response);
});

/* Assign `notify()` as a listener to messages from the content script.
   Calls "notify()", if a message from content script was received */
browser.runtime.onMessage.addListener(notify);





