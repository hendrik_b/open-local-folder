/*
If the click was on a link, send a message to the background page.
The message contains the link's URL.
*/
function notifyExtension(e) {
  var target = e.target;
  if (e.button != 0) return;
  while ((target.tagName != "A" || !target.href) && target.parentNode) {
    target = target.parentNode;
  }
  if (target.tagName != "A") return;
	
	console.log("content script sending message");
  browser.runtime.sendMessage({"url": target.href});
}

//Add notifyExtension() as a listener to click events. */
window.addEventListener("click", notifyExtension);

// Shows that this extension has been installed
document.body.classList.add("local-folder-addon-installed");

